/*******************************************************************************
 * This software is copyright (c) 2013 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www-daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Thorsten Vitt
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.services.collatexwrap.collatexwrapservice;

import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response.StatusType;
import javax.ws.rs.ext.ExceptionMapper;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import org.codehaus.jettison.json.JSONException;
import org.springframework.web.util.HtmlUtils;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;

/*******************************************************************************
 * PACKAGE
 * 		info.textgrid.services.collatexwrap.collatexwrapservice
 * 
 * FILE
 *	 	CollateXWrapServiceGenericExceptionWrapper.java
 *
 *******************************************************************************
 * TODOLOG
 * 
 * 	TODO	Fix generic exeption mapper!
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 	2013-05-21	Funk	Copied from TGCrudServiceGenericExceptionWrapper.java
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * Writes exception information in HTML bodies.
 * </p>
 * 
 * @author Thorsten Vitt
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2013-05-22
 * @since 2013-03-07
 ******************************************************************************/

public class CollateXWrapServiceGenericExceptionMapper implements
		ExceptionMapper<Exception> {

	public static final Map<Class<? extends Exception>, Response.Status>	STATUS_MAP		= ImmutableMap
																									.<Class<? extends Exception>, Response.Status> builder()
																									.put(TransformerException.class,
																											Status.BAD_REQUEST)
																									.put(IOException.class,
																											Status.INTERNAL_SERVER_ERROR)
																									.put(IoFault.class,
																											Status.INTERNAL_SERVER_ERROR)
																									.put(JSONException.class,
																											Status.BAD_REQUEST)
																									.put(XMLStreamException.class,
																											Status.BAD_REQUEST)
																									.build();

	private static final String												HTML_TEMPLATE	= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
																									+ "<!DOCTYPE html>\n"
																									+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
																									+ "    <head>\n"
																									+ "        <title>{0} {1}</title>\n"
																									+ "        <style type=\"text/css\">'\n"
																									+ "            h1 { border-bottom: 2px solid #3081c2; }\n"
																									+ "            .details { color: gray; }\n"
																									+ "        '</style>\n"
																									+ "    </head>\n"
																									+ "    <body>\n"
																									+ "        <img src=\"http://textgridrep.de/images/textgridrep_logo.png\" alt=\"TextGrid Repository Logo\"/><br/>"
																									+ "        <h1>{0} {1}</h1>\n"
																									+ "        <p class=\"message\">{2}</p>\n"
																									+ "        <div class=\"details\">\n"
																									+ "            <pre>\n"
																									+ "{3}\n"
																									+ "            </pre>\n"
																									+ "        </div>\n"
																									+ "    </body>\n"
																									+ "</html>";

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	@Override
	public Response toResponse(final Exception exception) {

		Status status;
		String message;
		if (exception instanceof WebApplicationException
				&& exception.getCause() != null) {
			status = STATUS_MAP.get(exception.getCause().getClass());
			if (status == null) {
				status = Status
						.fromStatusCode(((WebApplicationException) exception)
								.getResponse().getStatus());
			}
			message = exception.getCause().getLocalizedMessage();
		} else {
			status = STATUS_MAP.get(exception.getClass());
			message = exception.getLocalizedMessage();
		}
		if (status == null) {
			status = Status.INTERNAL_SERVER_ERROR;
		}

		final Response response = toResponse(status, message,
				Throwables.getStackTraceAsString(exception));
		return response;
	}

	/**
	 * <p>
	 * Returns a fancy HTML-formatted response for the given parameters.
	 * </p>
	 * 
	 * @param status
	 *            the status for which the response is to be generated.
	 * @param message
	 *            an additional informative message. Will be HTML-escaped and
	 *            displayed below the status msg
	 * @param detail
	 *            a detail message, e.g., a stack trace. Will be HTML-escaped
	 *            and displayed in a preformatted way below the status msg.
	 * @return the generated response.
	 */
	public static Response toResponse(final StatusType status,
			final String message, final String detail) {

		final ResponseBuilder builder = Response.status(status);
		builder.type(MediaType.APPLICATION_XHTML_XML_TYPE);
		builder.entity(prepareXHTMLMessage(status, message, detail));
		return builder.build();
	}

	/**
	 * @param status
	 *            see toResponse.
	 * @param message
	 *            see toResponse.
	 * @param detail
	 *            see toResponse.
	 * @return the prepared XHTML message.
	 */
	public static String prepareXHTMLMessage(final StatusType status,
			final String message, final String detail) {
		return MessageFormat.format(HTML_TEMPLATE, status.getStatusCode(),
				status.getReasonPhrase(), HtmlUtils.htmlEscapeHex(message),
				HtmlUtils.htmlEscapeHex(detail));
	}

}
