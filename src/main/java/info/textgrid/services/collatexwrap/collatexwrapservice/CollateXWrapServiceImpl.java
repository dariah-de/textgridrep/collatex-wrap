/*******************************************************************************
 * This software is copyright (c) 2013 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www-daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.services.collatexwrap.collatexwrapservice;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.adaptormanager.AdaptorManager;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TextGridMimetypes;
import info.textgrid.utils.httpclient.TGHttpClient;
import info.textgrid.utils.httpclient.TGHttpResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.TransformerException;

import org.apache.commons.io.input.NullInputStream;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/*******************************************************************************
 * PACKAGE
 * 			info.textgrid.services.collatexwrap
 * 
 * FILE
 * 			CollateXWrapServiceImpl.java
 *
 *******************************************************************************
 * TODOLOG
 * 
 * 	TODO	Do refactor the responseMimetype decision flow!
 * 	TODO	Add method to transform JSON to table HTML!
 *	TODO	Use streaming whereever possible!
 * 
 *******************************************************************************
 * CHANGELOG
 *
 *	2013-12-06	Funk	Added some missing @return annotations.
 *	2013-08-08	Funk	Fixed NPE if plaintext files and splitTag combination.
 *	2013-06-15	Funk	Removed empty witness check, CollateX-Server V1.4 again
 *						is accepting empty witnesses.
 *	2013-05-29	Funk	Adapted to new CollateX server 05/29/2013.
 *	2013-05-27	Funk	Added CXF Error Responses.
 *	2013-05-24	Funk	CSS is located in TG-rep-Browser, please see there.
 *	2013-05-17	Funk	Re-assembled big files.
 *	2013-05-10	Funk	Moved some code to private methods.
 *						Removed the typeMap, we do use the metadataMap instead
 *						for getting the object's mimetype.
 *	2013-04-26	Funk	Added Java Exceptions to methods instead of TG
 *						exceptions.
 *	2013-03-28	Funk	Updated CXF, Spring, and JSON libs.
 *	2013-02-07	Funk	Added many more things.
 *	2012-12-13	Funk	Implemented collate method.
 *	2012-11-29	Funk	First version.
 *
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * <b>The CollateXWrapService implementation class</b>
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2013-12-11
 * @since 2012-11-29
 ******************************************************************************/

public class CollateXWrapServiceImpl {

	// **
	// STATIC FINALS
	// **

	private static final String					COLLATEXWRAP_LOGGER			= "CXW";
	private static final long					LOGFILE_DELAY				= 60000l;

	private static final Long					HOR_MILLIS					= 3600000l;
	private static final Long					MIN_MILLIS					= 60000l;
	private static final Long					SEC_MILLIS					= 1000l;

	private static final String					PROPERTY_LOG4J_CONFIGFILE	= "LOG4J_CONFIGFILE";
	private static final String					PROPERTY_TGCRUD_ENDPOINT	= "TGCRUD_ENDPOINT";
	private static final String					PROPERTY_COLLATEX_ENDPOINT	= "COLLATEX_ENDPOINT";
	private static final String					PROPERTY_COLLATEX_TIMEOUT	= "COLLATEX_TIMEOUT";

	private static final String					LOGGER_RESPONSE				= "[rsp] ";
	private static final String					LOGGER_REQUEST				= "[rqs] ";
	private static final String					LOGGER_MIMETYPE				= "[mmt] ";
	private static final String					LOGGER_PATH					= "[pth] ";
	private static final String					LOGGER_TIMEOUT				= "[tmt] ";
	private static final String					LOGGER_JSON					= "[jsn] ";
	private static final String					LOGGER_EXECUTE				= "[exc] ";
	private static final String					LOGGER_ERROR				= "[err] ";

	private static final int					LOG_CONTENT_LENGTH			= 250;
	private static final int					ALL_SIGILS_WIDTH			= 100;
	private static final int					JSON_INDENTATION_FACTOR		= 4;
	private static final String					TEMPFILE_PREFIX				= ".";
	private static final String					TEMPFILE_SUFFIX				= ".xml";
	private static final String					FOR_URI_STRING				= " for URI ";

	/**
	 * <p>
	 * Some method strings.
	 * </p>
	 */
	private static final String					MAIN						= "MAIN";
	private static final String					GETVERSION					= "GETVERSION";
	private static final String					COLLATE						= "COLLATE";

	// **
	// CLASS VARIABLES
	// **

	/**
	 * <p>
	 * Create map to handle input streams and content types.
	 * </p>
	 */
	private Map<String, InputStream>			streamMap					= new HashMap<String, InputStream>();
	private Map<String, MetadataContainerType>	metadataMap					= new HashMap<String, MetadataContainerType>();
	private Map<String, List<File>>				tempfileMap					= new HashMap<String, List<File>>();

	private long								overallDuration;

	// **
	// STATICS
	// **

	/**
	 * <p>
	 * The CollateXWrap configuration file and properties object. Uses a Java
	 * properties file.
	 * </p>
	 */
	private Properties							configuration				= new Properties();

	/**
	 * <p>
	 * The tgcrud and collatex client stubs.
	 * </p>
	 */
	private URL									tgcrudEndpoint				= null;
	private TGHttpClient						tgcrudClient				= null;
	private URL									collatexEndpoint			= null;											;
	private TGHttpClient						collatexClient				= null;

	/**
	 * <p>
	 * The logger logs all information with the given loglevel from the
	 * properties file. Please configure the log4j file as stated in the
	 * properties configuration file.
	 * </p>
	 */
	private static volatile Logger				collatexWrapLogger			= null;

	// **
	// CONSTRUCTORS
	// **

	/**
	 * @param theConfLocation
	 * @throws IOException
	 */
	public CollateXWrapServiceImpl(String theConfLocation) throws IOException {

		// Set method string for logging.
		String method = MAIN + "\t";

		// Get properties file.
		this.configuration.load(new FileInputStream(new File(theConfLocation)));

		// Get the log4j logger.
		collatexWrapLogger = Logger.getLogger(COLLATEXWRAP_LOGGER);

		// Set the log4j properties from the config file given in the
		// properties file, and watch that file.
		String logConf = this.configuration
				.getProperty(PROPERTY_LOG4J_CONFIGFILE);

		if (logConf != null && !logConf.equals("")) {
			PropertyConfigurator.configureAndWatch(logConf, LOGFILE_DELAY);
		} else {
			throw new FileNotFoundException(
					"CollateX-Wrap log4j config file not found: " + logConf);
		}

		// Do first log.
		String version = CollateXWrapServiceVersion.VERSION + "-"
				+ CollateXWrapServiceVersion.BUILDDATE + "-"
				+ CollateXWrapServiceVersion.BUILDNAME;
		collatexWrapLogger.info(method + "service version " + version
				+ " started with logger " + COLLATEXWRAP_LOGGER);

		// Get the tgcrud client once.
		if (this.tgcrudClient == null) {
			this.tgcrudEndpoint = new URL(
					this.configuration.getProperty(PROPERTY_TGCRUD_ENDPOINT));
			this.tgcrudClient = new TGHttpClient(
					this.tgcrudEndpoint.toExternalForm());

			collatexWrapLogger.info(method
					+ "initialized tgcrud client from service: "
					+ this.tgcrudEndpoint.toString());
		}

		// Get CollateX client once, set timeout according to config.
		if (this.collatexClient == null) {
			this.collatexEndpoint = new URL(
					this.configuration.getProperty(PROPERTY_COLLATEX_ENDPOINT));
			this.collatexClient = new TGHttpClient(
					this.collatexEndpoint.toExternalForm());
			int serviceTimeout = Integer.valueOf(this.configuration
					.getProperty(PROPERTY_COLLATEX_TIMEOUT));
			this.collatexClient.setClientParameter(
					CoreConnectionPNames.SO_TIMEOUT, serviceTimeout);

			collatexWrapLogger
					.debug(method
							+ LOGGER_TIMEOUT
							+ this.collatexClient
									.getClientParameter(CoreConnectionPNames.SO_TIMEOUT));
		}
	}

	/**
	 * <p>
	 * <b>CollateX Wrap service method #GETVERSION</b>
	 * </p>
	 * 
	 * <p>
	 * <b>#GETVERSION</b> returns the current version of the service.</b>
	 * </p>
	 * 
	 * @return a version string
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/version")
	public static String getVersion() {

		// Set method string for logging.
		String method = "#" + GETVERSION + "\t";

		// Assemble version string.
		String result = CollateXWrapServiceVersion.VERSION + "-"
				+ CollateXWrapServiceVersion.BUILDDATE + "-"
				+ CollateXWrapServiceVersion.BUILDNAME;

		// Do log.
		collatexWrapLogger.info(method + result);

		// Return response.
		return result;
	}

	/**
	 * <p>
	 * The main collate method.
	 * </p>
	 * 
	 * @param sessionId
	 * @param preAdaptorUri
	 * @param postAdaptorUri
	 * @param splitTags
	 * @param uris
	 * @param responseMimetype
	 * @param algorithm
	 * @param comparator
	 * @param distance
	 * @param joined
	 * @return an HTTP response containing the expected (or error) response
	 */
	@GET
	@Path("/collate")
	// Not specifiable, because we can have different response types.
	// @Produces(MediaType.TEXT_XML)
	public Response collate(@QueryParam("sessionId") final String sessionId,
			@QueryParam("preAdaptorUri") final String preAdaptorUri,
			@QueryParam("postAdaptorUri") final String postAdaptorUri,
			@QueryParam("splitTag") final List<String> splitTags,
			@QueryParam("uri") final List<String> uris,
			@QueryParam("responseMimetype") final String responseMimetype,
			@QueryParam("algorithm") final String algorithm,
			@QueryParam("comparator") final String comparator,
			@QueryParam("distance") final int distance,
			@QueryParam("joined") final boolean joined) {

		// ----------------------------------------------------------------------
		// Some starting issues done here.
		// ----------------------------------------------------------------------

		// Set start time.
		this.overallDuration = System.currentTimeMillis();

		// Set method string for logging.
		String method = "#" + COLLATE + ".collate()\t";

		// Reset tempfile list.
		// FIXME Try using not a global list!
		this.tempfileMap.clear();

		InputStream result = null;

		// Do log.
		collatexWrapLogger.debug(method + "[sid] '" + sessionId + "'");
		collatexWrapLogger.debug(method + "[pre] '" + preAdaptorUri + "'");
		collatexWrapLogger.debug(method + "[pst] '" + postAdaptorUri + "'");
		collatexWrapLogger.debug(method + "[stg] '" + splitTags + "'");
		collatexWrapLogger.debug(method + "[lst] '" + uris.toString() + "'");
		collatexWrapLogger.debug(method + "[rmt] '" + responseMimetype + "'");
		collatexWrapLogger.debug(method + "[alg] '" + algorithm + "'");
		collatexWrapLogger.debug(method + "[cmp] '" + comparator + "'");
		collatexWrapLogger.debug(method + "[dts] '" + distance + "'");
		collatexWrapLogger.debug(method + "[jnd] '" + joined + "'");

		// ----------------------------------------------------------------------
		// Do check for input parameters.
		// ----------------------------------------------------------------------

		// Allumfassendes try to return nice error responses using the
		// GenericExceptionMapper...
		try {
			// Have we got at least two URIs?
			if (uris.size() < 2) {
				String message = "please give at least two uris for the collate process!!";
				collatexWrapLogger.error(method + LOGGER_ERROR + message);
				throw new IoFault(message);
			}

			// ----------------------------------------------------------------------
			// Do get the documents from TG-crud.
			// ----------------------------------------------------------------------

			// Create a overall URI list and put pre- and postprocessing URIs
			// into it.
			List<String> completeUris = new ArrayList<String>();
			completeUris.addAll(uris);
			if (preAdaptorUri != null && !preAdaptorUri.equals("")) {
				completeUris.add(preAdaptorUri);
			}
			if (postAdaptorUri != null && !postAdaptorUri.equals("")) {
				completeUris.add(postAdaptorUri);
			}

			// ----------------------------------------------------------------------
			// Get title from metadata for table headings.
			// ----------------------------------------------------------------------

			getMetadata(completeUris, sessionId);

			// ----------------------------------------------------------------------
			// Get data.
			// ----------------------------------------------------------------------

			getData(completeUris, sessionId);

			// ----------------------------------------------------------------------
			// Split XML files, if split tag is given and we have XML mimetype.
			// ----------------------------------------------------------------------

			if (splitTags != null && !splitTags.isEmpty()) {
				splitXmlFiles(completeUris, splitTags);
			}

			// ----------------------------------------------------------------------
			// Do remove the XML tags using XSLT adaptors.
			// ----------------------------------------------------------------------

			if (preAdaptorUri != null && !preAdaptorUri.equals("")) {
				usePreAdaptor(completeUris, preAdaptorUri, splitTags);
			}

			// Remove preadaptor URI from uriList.
			completeUris.remove(preAdaptorUri);

			// ----------------------------------------------------------------------
			// Send all of them to the CollateX server, if split tag is gioven
			// and temp file map is not empty.
			// ----------------------------------------------------------------------

			List<InputStream> resultList = new ArrayList<InputStream>();
			if (splitTags != null && !splitTags.isEmpty()
					&& !this.tempfileMap.isEmpty()) {
				resultList = concatenateCollateXResponses(completeUris,
						responseMimetype, algorithm, comparator, distance,
						joined);
			} else {
				resultList.add(sendToCollateX(completeUris, this.streamMap,
						responseMimetype, algorithm, comparator, distance,
						joined));
			}

			// ----------------------------------------------------------------------
			// Do post transform the CollateX response.
			// ----------------------------------------------------------------------

			// If we have a postprocessing adaptor URI and a response XML
			// mimetype, post-transform using the CollateX response.
			// FIXME Does not yet work with split tag feature!
			if (postAdaptorUri != null && !postAdaptorUri.equals("")
					&& responseMimetype.contains("xml")) {

				result = AdaptorManager.transform(result,
						this.streamMap.get(postAdaptorUri));

				collatexWrapLogger.info(method + LOGGER_EXECUTE
						+ "post-transformed result stream with adaptor "
						+ postAdaptorUri);
			}

			// If we have no postprocessing adaptor URI and a response text/html
			// mimetype, do convert to HTML.
			else if (responseMimetype.equals("text/html")
					&& (postAdaptorUri == null || postAdaptorUri.equals(""))) {
				result = transformToHTML(resultList);
			}

			// No post-transformation done.
			else {
				collatexWrapLogger.info(method + LOGGER_EXECUTE
						+ "no post-transformation done");
			}

			// ----------------------------------------------------------------------
			// Get result input stream out of the response list, if not existing
			// yet.
			// ----------------------------------------------------------------------

			// We only need to get the first result, because only for HTML
			// responses the result can be concatenated.
			if (result == null && !resultList.isEmpty()) {
				result = resultList.get(0);
			}

			// ----------------------------------------------------------------------
			// Build an HTTP response.
			// ----------------------------------------------------------------------

			// For now just change TEI mimetype to XML mimetype.
			// FIXME Is that really necesarry? Maybe returning the REAL
			// mimetypes would be nicer?
			String responseMimetypeToReturn = responseMimetype;
			if (responseMimetype.equals("application/graphml+xml")
					|| responseMimetype.equals("application/tei+xml")) {
				responseMimetypeToReturn = "text/xml";
			}

			ResponseBuilder rBuilder = Response.ok(result,
					responseMimetypeToReturn);

			// TODO Add header fields depending on response mimetypes. We want
			// all mimetypes NOT beeing text/html to force saving, don't we?
			// if (responseMimetype != "text/html") {
			// TODO Specify filenames and file extensions!
			// rBuilder.header("Content-Disposition",
			// "attachment; filename=FILENAME");
			// }

			collatexWrapLogger.info(method + LOGGER_RESPONSE
					+ "sent, mimetype: " + responseMimetype);

			// ----------------------------------------------------------------------
			// Return collated XML document.
			// ----------------------------------------------------------------------

			return rBuilder.build();

		} catch (IoFault e) {
			throw new WebApplicationException(e,
					Response.Status.INTERNAL_SERVER_ERROR);
		} catch (IOException e) {
			throw new WebApplicationException(e,
					Response.Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
		} catch (TransformerException e) {
			throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
		} catch (XMLStreamException e) {
			throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
		}
	}

	/**
	 * <p>
	 * Fill the metadata map with the object's metadata.
	 * </p>
	 * 
	 * @param theUriList
	 * @param theSessionId
	 * @throws IOException
	 */
	private void getMetadata(List<String> theUriList, String theSessionId)
			throws IOException {

		// Set method string for logging.
		String method = "#" + COLLATE + ".getMetadata()\t";

		// Do loop through all the objects and get metadata from TG-crud.
		for (String u : theUriList) {

			String request = u + "/metadata";
			String path = "?sessionId=" + theSessionId;

			TGHttpResponse response = this.tgcrudClient.get(request + path);

			int statusCode = response.getStatusCode();

			if (statusCode == HttpStatus.SC_OK) {
				BufferedHttpEntity entity = response.getBuffEntity();

				collatexWrapLogger.debug(method + LOGGER_MIMETYPE
						+ entity.getContentType());

				// Create metadata container.
				MetadataContainerType metadata = JAXB.unmarshal(
						entity.getContent(), MetadataContainerType.class);

				// Put metadata in map.
				this.metadataMap.put(u, metadata);

				collatexWrapLogger.info(method + LOGGER_RESPONSE
						+ response.getStatusCode() + "/"
						+ response.getReasonPhrase()
						+ ": metadata received from tgcrud "
						+ this.tgcrudEndpoint.toExternalForm() + FOR_URI_STRING
						+ u);

				this.streamMap.put(u, entity.getContent());

				// MetadataParseFault: 400 BAD REQUEST
				// IoFault: 500 INTERNAL SERVER ERROR
				// ProtocolNotImplementedFault: 400 BAD REQUEST
				// AuthFault: 401 UNAUTHORIZED

			} else {
				String errorMessage = method + LOGGER_RESPONSE
						+ response.getStatusCode() + "/"
						+ response.getReasonPhrase()
						+ ": error receiving metadata from tgcrud "
						+ this.tgcrudEndpoint.toExternalForm() + FOR_URI_STRING
						+ u;
				collatexWrapLogger.error(errorMessage);

				throw new IOException(errorMessage);
			}
		}
	}

	/**
	 * <p>
	 * Fill the stream map with the object's data.
	 * </p>
	 * 
	 * @param theUriList
	 * @param theSessionId
	 * @throws IOException
	 */
	private void getData(List<String> theUriList, String theSessionId)
			throws IOException {

		// Set method string for logging.
		String method = "#" + COLLATE + ".getData()\t";

		// Do loop through all the objects and get them from TG-crud.
		for (String u : theUriList) {

			String request = u + "/data";
			String path = "?sessionId=" + theSessionId;

			collatexWrapLogger.info(method + LOGGER_REQUEST
					+ this.tgcrudEndpoint.toExternalForm() + request);
			collatexWrapLogger.debug(method + LOGGER_PATH + path);

			TGHttpResponse response = this.tgcrudClient.get(request + path);

			int statusCode = response.getStatusCode();

			if (statusCode == HttpStatus.SC_OK) {
				BufferedHttpEntity entity = response.getBuffEntity();

				collatexWrapLogger.debug(method + LOGGER_MIMETYPE
						+ entity.getContentType());
				collatexWrapLogger.info(method + LOGGER_RESPONSE
						+ response.getStatusCode() + "/"
						+ response.getReasonPhrase()
						+ ": data received from tgcrud "
						+ this.tgcrudEndpoint.toExternalForm() + FOR_URI_STRING
						+ u);

				this.streamMap.put(u, entity.getContent());

			} else {
				String errorMessage = method + LOGGER_RESPONSE
						+ response.getStatusCode() + "/"
						+ response.getReasonPhrase()
						+ ": error receiving data from tgcrud "
						+ this.tgcrudEndpoint.toExternalForm() + FOR_URI_STRING
						+ u;

				collatexWrapLogger.error(errorMessage);

				throw new IOException(errorMessage);
			}
		}
	}

	/**
	 * <p>
	 * Splits each XML file into pieces due to given tag names and amount.
	 * </p>
	 * 
	 * @param theUriList
	 * @param theSplitTags
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	private void splitXmlFiles(List<String> theUriList,
			List<String> theSplitTags) throws IOException, XMLStreamException {

		// Set method string for logging.
		String method = "#" + COLLATE + ".splitXmlFiles()\t";

		for (String u : theUriList) {
			String contentType = this.metadataMap.get(u).getObject()
					.getGeneric().getProvided().getFormat();

			// Test if we have an XML mimetype here.
			if (contentType != null
					&& contentType.equals(TextGridMimetypes.XML)) {

				collatexWrapLogger.debug(method + LOGGER_MIMETYPE
						+ "detected mimetype " + contentType
						+ ", processing XML file splitting");

				// If we have XML mimetype,

				XMLInputFactory ifac = XMLInputFactory.newInstance();

				XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
				outputFactory.setProperty(
						"javax.xml.stream.isRepairingNamespaces", Boolean.TRUE);

				InputStream stream2split = this.streamMap.get(u);
				XMLEventReader eread = ifac.createXMLEventReader(stream2split);

				// Assemble split tags on QName list.
				List<QName> names = new ArrayList<QName>();
				for (String splitTag : theSplitTags) {
					names.add(new QName(splitTag));
				}

				try {
					while (true) {
						XMLEvent event = eread.nextEvent();
						if (event.isStartElement()) {
							StartElement element = event.asStartElement();
							if (names.contains(element.getName())) {
								writeToTempFile(outputFactory, eread, event, u,
										element);
							}
						}
						if (event.isEndDocument()) {
							break;
						}
					}
				} finally {
					eread.close();
				}
			}
		}
	}

	/**
	 * <p>
	 * Write XML parts to temp file.
	 * </p>
	 * 
	 * @param theOutputFactory
	 * @param theReader
	 * @param theStartEvent
	 * @param theUri
	 * @param theElement
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	private void writeToTempFile(XMLOutputFactory theOutputFactory,
			XMLEventReader theReader, XMLEvent theStartEvent, String theUri,
			StartElement theElement) throws XMLStreamException, IOException {

		// Set method string for logging.
		String method = "#" + COLLATE + ".writeToTempFile()\t";

		StartElement element = theStartEvent.asStartElement();
		QName name = element.getName();

		int stack = 1;

		File tempFile = File.createTempFile(theUri + TEMPFILE_PREFIX,
				TEMPFILE_SUFFIX);

		if (this.tempfileMap.get(theUri) == null) {
			this.tempfileMap.put(theUri, new ArrayList<File>());
		}
		this.tempfileMap.get(theUri).add(tempFile);

		// Print XML beginning things to temp file.

		collatexWrapLogger.debug(method + LOGGER_PATH
				+ " part-tempfile created [" + element.getName().getLocalPart()
				+ "]: " + tempFile.getName());

		FileWriter fw = new FileWriter(tempFile);
		fw.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n");
		XMLEventWriter writer = theOutputFactory.createXMLEventWriter(fw);
		writer.add(element);

		while (true) {
			XMLEvent event = theReader.nextEvent();
			if (event.isStartElement()
					&& event.asStartElement().getName().equals(name)) {
				stack++;
			}
			if (event.isEndElement()) {
				EndElement end = event.asEndElement();
				if (end.getName().equals(name)) {
					stack--;
					if (stack == 0) {
						writer.add(event);
						break;
					}
				}
			}
			writer.add(event);
		}
		writer.close();
	}

	/**
	 * <p>
	 * Transform the data using the XSLT preAdaptor.
	 * </p>
	 * 
	 * @param theUriList
	 * @param thePreAdaptorUri
	 * @param theSplitTags
	 * @throws IOException
	 * @throws TransformerException
	 */
	private void usePreAdaptor(final List<String> theUriList,
			final String thePreAdaptorUri, final List<String> theSplitTags)
			throws IOException, TransformerException {

		// Set method string for logging.
		String method = "#" + COLLATE + ".usePreAdaptor()\t";

		for (String u : theUriList) {
			String contentType = this.metadataMap.get(u).getObject()
					.getGeneric().getProvided().getFormat();

			// Test if we have an XML mimetype here.
			if (contentType != null && !contentType.equals("")
					&& contentType.equals(TextGridMimetypes.XML)) {

				collatexWrapLogger.debug(method + LOGGER_MIMETYPE
						+ "detected mimetype " + contentType
						+ ", processing pre-transformation with adaptor "
						+ thePreAdaptorUri);

				// If XML file has been splitted, replace the files with the
				// adapted ones.
				if (theSplitTags != null && !theSplitTags.isEmpty()) {

					// Create new temp file list.
					List<File> newTempFileList = new ArrayList<File>();

					// Loop over the temp file list, and process
					// pre-adaptor.
					for (File tempFile : this.tempfileMap.get(u)) {

						// Create new temp file.
						File newTempFile = File.createTempFile(u
								+ TEMPFILE_PREFIX, TEMPFILE_SUFFIX);

						// Process pre-adaptor (e.g. strip all the XML
						// tags).
						// TODO Avoid using transform() (inputStream >>
						// outputStream) and again outputStream >> inputStream
						// here!!
						InputStream in = AdaptorManager.transform(
								new FileInputStream(tempFile),
								this.streamMap.get(thePreAdaptorUri));
						FileOutputStream out = new FileOutputStream(newTempFile);
						IOUtils.copyAndCloseInput(in, out);
						out.close();

						// Add new temp file to new temp file list.
						newTempFileList.add(newTempFile);

						collatexWrapLogger.info(method + LOGGER_EXECUTE
								+ "pre-transformed part-tempfile "
								+ tempFile.getName() + ", replaced with "
								+ newTempFile.getName());

						// Rewind the adaptor stream.
						this.streamMap.get(thePreAdaptorUri).reset();
					}

					// Replace temp file list to temp file map.
					if (this.tempfileMap.containsKey(u)) {
						this.tempfileMap.remove(u);
						this.tempfileMap.put(u, newTempFileList);
					}
				}

				// If not, replace the single XML file with the adapted
				// stream.
				else {

					// Process pre-adaptor (e.g. strip all the XML things).
					InputStream out = AdaptorManager.transform(
							this.streamMap.get(u),
							this.streamMap.get(thePreAdaptorUri));

					// Replace stream!
					if (this.streamMap.containsKey(u)) {
						this.streamMap.remove(u);
						this.streamMap.put(u, out);
					}

					collatexWrapLogger.info(method + LOGGER_EXECUTE
							+ "pre-transformed object xml " + u);

					// Rewind the adaptor stream.
					this.streamMap.get(thePreAdaptorUri).reset();
				}
			}
		}
	}

	/**
	 * <p>
	 * Fet an inputStream from the CollateX server for a group of objects.
	 * </p>
	 * 
	 * @param theUris
	 * @param theMap
	 * @param theResponseMimetype
	 * @param theAlgorithm
	 * @param theComparator
	 * @param theDistance
	 * @param theJoined
	 * @return InputStream from CollateX server
	 * @throws IOException
	 * @throws JSONException
	 */
	private InputStream sendToCollateX(final List<String> theUris,
			final Map<String, InputStream> theMap,
			final String theResponseMimetype, final String theAlgorithm,
			final String theComparator, final int theDistance,
			final boolean theJoined) throws IOException, JSONException {

		// Change responseMimetype to JSON, if HTML is sent (for WE return HTML,
		// but the CollateX Server returns JSON).
		String responseMimetypeToReturn = theResponseMimetype;
		if (theResponseMimetype.equals("text/html")) {
			responseMimetypeToReturn = "application/json";
		}

		// Set method string for logging.
		String method = "#" + COLLATE + ".sendToCollateX()\t";

		Header headers[] = new Header[2];
		headers[0] = new BasicHeader("Content-Type", "application/json");
		headers[1] = new BasicHeader("Accept", responseMimetypeToReturn);

		// FIXME STREAM HERE!!!
		StringEntity entity = new StringEntity(buildJson(theUris, theMap,
				theAlgorithm, theComparator, theDistance, theJoined));

		String request = "collate/";

		collatexWrapLogger.info(method + LOGGER_REQUEST
				+ this.collatexEndpoint.toExternalForm() + request);

		TGHttpResponse response = this.collatexClient.post(request, entity,
				headers);

		int statusCode = response.getStatusCode();

		if (statusCode == HttpStatus.SC_OK) {
			collatexWrapLogger.info(method + LOGGER_RESPONSE
					+ response.getStatusCode() + "/"
					+ response.getReasonPhrase()
					+ ": data received from collatex server");
		} else {
			collatexWrapLogger.error(method + LOGGER_RESPONSE
					+ response.getStatusCode() + "/"
					+ response.getReasonPhrase()
					+ ": error receiving data from collatex server");
		}

		return response.getBuffEntity().getContent();
	}

	/**
	 * <p>
	 * Send data to the CollateX server and concatenate the splitted CollateX
	 * responses.
	 * </p>
	 * 
	 * @param theUris
	 * @param theResponseMimetype
	 * @param theAlgorithm
	 * @param theComparator
	 * @param theDistance
	 * @param theJoined
	 * @return a list of CollateX HTML responses
	 * @throws IOException
	 * @throws JSONException
	 */
	private List<InputStream> concatenateCollateXResponses(
			final List<String> theUris, final String theResponseMimetype,
			final String theAlgorithm, final String theComparator,
			final int theDistance, final boolean theJoined) throws IOException,
			JSONException {

		List<InputStream> result = new ArrayList<InputStream>();

		// Set method string for logging.
		String method = "#" + COLLATE + ".concatenateCollateXResponses()\t";

		// Compute maximum of temp file maps.
		// TODO This MUST be possible to implement simpler! I'll just try it
		// tomorrow!
		int tempfileCount[] = new int[theUris.size()];
		int max = 0;
		for (int i = 0; i < theUris.size(); i++) {
			tempfileCount[i] = this.tempfileMap.get(theUris.get(i)).size();
			if (i > 0) {
				max = Math.max(tempfileCount[i - 1], tempfileCount[i]);
			}

			collatexWrapLogger.debug(method + "[tfc] " + theUris.get(i) + ": "
					+ tempfileCount[i]);
		}

		collatexWrapLogger.debug(method + "[max] " + max);

		// Now we loop over the maximum of available temp files, and fill the
		// list of input streams for each group of temp file groups to send to
		// the CollateX server.
		for (int i = 0; i < max; i++) {

			// Loop over all the URIs, do re-use the streamMap for every URI.
			// TODO Not nice to reuse the streamMap, but useful at the moment...
			Map<String, InputStream> tempMap = new HashMap<String, InputStream>();
			for (String u : theUris) {

				// Only if the tempfileMap has got element i, add it to the
				// tempMap.
				if (i < this.tempfileMap.get(u).size()) {
					tempMap.put(u, new FileInputStream(this.tempfileMap.get(u)
							.get(i)));
				}
				// Else add a null input stream for filling up the map.
				else {
					tempMap.put(u, new NullInputStream(0l));
				}
			}

			// Now send stream list to CollateX.
			result.add(sendToCollateX(theUris, tempMap, theResponseMimetype,
					theAlgorithm, theComparator, theDistance, theJoined));
		}

		// And return the list of CollateX results.
		return result;
	}

	/**
	 * <p>
	 * Transforms the JSON output of the CollateX server into HTML tables, for
	 * each InputStream one table is generated.
	 * </p>
	 * 
	 * @param theResults
	 * @return an InputStream with the HTML collatex result
	 * @throws JSONException
	 * @throws IOException
	 */
	private InputStream transformToHTML(final List<InputStream> theResults)
			throws JSONException, IOException {

		// Set method string for logging.
		String method = "#" + COLLATE + ".transformToHTML()\t";

		// FIXME Use some whitespace handling to display whitespaces in HTML
		// table?

		// Create buffer and add headers.
		StringBuffer htmlResult = new StringBuffer();
		htmlResult
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
						+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
						+ "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
						+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">"
						+ "<body style=\"font-family: Arial, Helvetica, sans-serif;\">");

		// Create Tables for each incoming input stream.
		int partCount = 0;
		for (InputStream stream : theResults) {

			partCount++;

			// Add explanation.
			htmlResult
					.append("<p align=\"right\"><b class=\"invariant\">invariant</b>, ");
			htmlResult.append("<b class=\"variant\">variant</b>, ");
			htmlResult.append("<b class=\"semi-invariant\">semi-invariant</b>");
			htmlResult.append("</p><br/>");

			htmlResult.append("<table class=\"collatex\">");

			// FIXME Do stream here!
			JSONObject o = new JSONObject(IOUtils.toString(stream, "UTF-8"));

			// Get witnesses.
			JSONArray witnesses = o.getJSONArray("witnesses");

			collatexWrapLogger.debug(method + LOGGER_JSON + "witnesses: "
					+ witnesses);

			// Add header, if result list has more then one result.
			if (theResults.size() > 1) {
				htmlResult.append("<tr><th colspan=" + witnesses.length()
						+ ">Part " + partCount + "</th></tr>");
			}

			// Add Title and TextGrid URI.
			htmlResult.append("<tr>");

			for (int i = 0; i < witnesses.length(); i++) {
				htmlResult.append("<th width=\""
						+ (ALL_SIGILS_WIDTH / witnesses.length())
						+ "%\" bgcolor=\"#eeeeee\"><b>"
						+ this.metadataMap.get(witnesses.get(i)).getObject()
								.getGeneric().getProvided().getTitle().get(0)
						+ " (" + witnesses.get(i) + ")</b></th>");
			}

			htmlResult.append("</tr>");

			// Get table.
			JSONArray table = o.getJSONArray("table");

			// Get inner arrays.
			for (int i = 0; i < table.length(); i++) {

				htmlResult.append("<tr>");

				JSONArray innerArray = table.getJSONArray(i);

				// Get inner strings.
				HashSet<String> var = new HashSet<String>();
				String ia[] = new String[innerArray.length()];
				for (int j = 0; j < innerArray.length(); j++) {
					if (!innerArray.isNull(j)) {
						JSONArray innerinnerArray = innerArray.getJSONArray(j);

						String kString = "";
						for (int k = 0; k < innerinnerArray.length(); k++) {
							// kString += new
							// String(innerinnerArray.getString(k)
							// .getBytes("UTF-8"), "UTF-8") + " ";
							kString += new String(innerinnerArray.getString(k)
									.getBytes("UTF-8"), "UTF-8");
						}

						ia[j] = kString.trim();
						var.add(ia[j]);
					} else {
						ia[j] = "";
						var.add(ia[j]);
					}

					// DEBUG OUTPUT!
					// collatexWrapLogger.debug(method + "[jsn] " + "kstring[" +
					// j
					// + "]: ‘" + ia[j] + "'");
					// DEBUG OUTPUT!

				}

				// DEBUG OUTPUT!
				// Iterator<String> iter = var.iterator();
				// while (iter.hasNext()) {
				// collatexWrapLogger.debug("####    '" + iter.next() + "'");
				// }
				// collatexWrapLogger.debug("####    ---- " + var.size() + " "
				// + var.contains("") + " ----");
				// DEBUG OUTPUT!

				// Check for variant/invariant, default is variant here.
				String erg = "variant";
				// Only one entry in HashMap >> invariant!
				if (var.size() == 1) {
					erg = "invariant";
				}
				// An empty string and all others are invariant >>
				// semi-invariant
				else if (var.contains("") && var.size() == 2) {
					erg = "semi-invariant";
				}

				// Visualize. Create HTML.
				for (int j = 0; j < innerArray.length(); j++) {
					htmlResult.append("<td class=\"" + erg + "\">");
					htmlResult.append(ia[j]);
					htmlResult.append("</td>");
				}

				htmlResult.append("</tr>");
			}

			htmlResult.append("</table>");
		}

		// Add duration and explenation and version string.
		String duration = getDuration(System.currentTimeMillis()
				- this.overallDuration);
		String version = CollateXWrapServiceVersion.VERSION + "-"
				+ CollateXWrapServiceVersion.BUILDDATE + "-"
				+ CollateXWrapServiceVersion.BUILDNAME;
		htmlResult.append("<br/><p id=\"duration\">...collated in " + duration
				+ " by CollateX-Wrap Service " + version + "</p>");

		htmlResult.append("</body></html>");

		return new ByteArrayInputStream(htmlResult.toString().getBytes());
	}

	/**
	 * <p>
	 * Build a JSON string out of the streams to collate.
	 * </p>
	 * 
	 * @param theUriList
	 * @param theMap
	 * @param theAlgorithm
	 * @param theComparator
	 * @param theDistance
	 * @param joined
	 * @return a Json string
	 * @throws IOException
	 * @throws JSONException
	 */
	private static String buildJson(final List<String> theUriList,
			final Map<String, InputStream> theMap, final String theAlgorithm,
			final String theComparator, final int theDistance,
			final boolean joined) throws IOException, JSONException {

		// Set method string for logging.
		String method = "#" + COLLATE + ".buildJson()\t";

		// Set some default values.
		String comparator = theComparator;
		if (theComparator == null || theComparator.equals("")) {
			comparator = "equality";
		}

		// FIXME Use STREAMING here!!

		// Create main JSON object and log JSON object.
		JSONObject jo = new JSONObject();
		JSONObject logJo = new JSONObject();

		// Create JSON array for all witnesses.
		JSONArray witnesses = new JSONArray();
		JSONArray logWitnesses = new JSONArray();

		for (int i = 0; i < theUriList.size(); i++) {

			String u = theUriList.get(i);

			collatexWrapLogger.debug(method + LOGGER_JSON
					+ "creating json string for " + u);

			// Create single JSON witness and JSON log witness.
			JSONObject witness = new JSONObject();
			JSONObject logWitness = new JSONObject();
			witness.put("id", u);
			logWitness.put("id", u);

			String content = IOUtils.readStringFromStream(theMap.get(u));

			// Log only beginning and ending if content is larger than 100
			// bytes.
			String logContent;
			if (content.length() > LOG_CONTENT_LENGTH) {
				logContent = content.substring(0, LOG_CONTENT_LENGTH)
						+ "   ---[ommitting "
						+ (content.length() - (2 * LOG_CONTENT_LENGTH))
						+ " bytes of content output]---   "
						+ content.substring(content.length()
								- LOG_CONTENT_LENGTH);
			} else {
				logContent = content;
			}

			// TODO Replace whitespaces using the XSLT preprocessing! It would
			// be more general that way!
			witness.put("content", content.replaceAll("[\\s]+", " ").trim());
			logWitness.put("content", logContent.replaceAll("[\\s]+", " ")
					.trim());

			// Add witness to witnesses.
			witnesses.put(witness);
			logWitnesses.put(logWitness);
		}

		// Add witnesses.
		jo.put("witnesses", witnesses);
		logJo.put("witnesses", logWitnesses);

		// Add algorithm, comparator, and distance.
		jo.put("algorithm", theAlgorithm);
		logJo.put("algorithm", theAlgorithm);

		// Create token comparator object.
		JSONObject joComp = new JSONObject();
		joComp.put("type", comparator);
		if (comparator.equals("levenshtein")) {
			joComp.put("distance", theDistance == 0 ? "1" : theDistance);
		}
		jo.put("tokenComparator", joComp);
		logJo.put("tokenComparator", joComp);

		// Create the joined object, if parameter applies.
		jo.put("joined", joined);
		logJo.put("joined", joined);

		collatexWrapLogger.debug(method + LOGGER_JSON
				+ logJo.toString(JSON_INDENTATION_FACTOR));
		collatexWrapLogger
				.debug(method + LOGGER_JSON + "returning json string");

		return jo.toString();
	}

	/***************************************************************************
	 * <p>
	 * Return the duration with milliseconds per default and use some default
	 * unit strings.
	 * </p>
	 * 
	 * @param duration
	 * @return duration of millies in a nice formatted time string
	 **************************************************************************/
	private static String getDuration(final long duration) {
		return getDurationInHours(duration, false, "less than a", "s", "hour",
				"minute", "second", "millisecond");
	}

	/***************************************************************************
	 * <p>
	 * Interpret and format a long as a duration. Useful for a perfomance check
	 * with two System.currentTimeMillis().
	 * </p>
	 * 
	 * @param duration
	 *            The time to display the duration for in milliseconds.
	 * @param showMils
	 *            Show the milliseconds or hide them.
	 * @param lessThanExpression
	 *            Puts a less than string before
	 * @param plural
	 *            Puts a string behind all plurals
	 * @param dispHor
	 *            The hour display string.
	 * @param dispMin
	 *            The minute display string.
	 * @param dispSec
	 *            The second display string.
	 * @param dispMil
	 *            The millisecond display string.
	 * @return String The duration in hours.
	 */
	private static String getDurationInHours(final long duration,
			final boolean showMils, final String lessThanExpression,
			final String plural, final String dispHor, final String dispMin,
			final String dispSec, final String dispMil) {

		String result = "";
		DecimalFormat df = new DecimalFormat("0");

		long hors = (duration / HOR_MILLIS); // get hours
		long rest = (duration % HOR_MILLIS); // get rest

		long mins = (rest / MIN_MILLIS); // get minutes
		rest = (rest % MIN_MILLIS); // get rest

		long secs = (rest / SEC_MILLIS); // get seconds
		rest = (rest % SEC_MILLIS); // get rest

		long mils = rest;

		if (hors >= 1) {
			result += df.format(hors) + " " + dispHor
					+ (hors > 1 ? plural + " " : " ");
		}
		if (mins >= 1) {
			result += df.format(mins) + " " + dispMin
					+ (mins > 1 ? plural + " " : " ");
		}
		if (secs >= 1) {
			result += df.format(secs) + " " + dispSec
					+ (secs > 1 ? plural + " " : " ");
		}
		if (mils >= 1 && showMils) {
			result += df.format(mils) + " " + dispMil
					+ (mils > 1 ? plural + " " : " ");
		}

		if (result.equals("")) {
			if (showMils) {
				result = lessThanExpression + " " + dispMil;
			} else {
				result = lessThanExpression + " " + dispSec;
			}
		}

		return result.trim();
	}

}
