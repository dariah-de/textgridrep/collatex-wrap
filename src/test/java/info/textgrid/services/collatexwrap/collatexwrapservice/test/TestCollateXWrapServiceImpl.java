/*******************************************************************************
 * This software is copyright (c) 2013 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www-daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.services.collatexwrap.collatexwrapservice.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import info.textgrid.utils.httpclient.TGHttpClient;
import info.textgrid.utils.httpclient.TGHttpResponse;

import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.params.CoreConnectionPNames;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/*******************************************************************************
 * PACKAGE
 * 			info.textgrid.services.collatexwrap.collatexwrapservice.test
 * 
 * FILE
 * 			TestCollateXWrapServiceImpl.java
 * 
 *******************************************************************************
 * TODOLOG
 * 
 * 	TODO	Try testing if a returned XML file is a collated one with correct
 * 			content, and NOT containing headers only!
 * 	TODO	Use log4j logger again!
 * 	
 *******************************************************************************
 * CHANGELOG
 * 
 * 	2013-11-07	Funk	Added some Juxta tests for creating text-and-color-
 * 						output in HTML.
 * 	2013-06-25	Funk	Added tests for empty witnesses, and MEDITE algorithm
 * 						for CollateX-Server V1.4
 * 	2013-04-30	Funk	Commented out the log4j logging, is not working at the
 * 						moment... using java.util.logging now...
 * 	2013-04-27	Funk	Added more test methods, added configuration via
 * 						properties file.
 * 	2013-04-26	Funk	Added tests for version and collate method (taken from
 * 						Test.java (now marked as deprecated).
 *	2012-12-13	Funk	First version.
 *
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * JUnit class for testing all the CollateXWrap service methods internally.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2014-08-04
 * @since 2012-12-13
 ******************************************************************************/

public class TestCollateXWrapServiceImpl {

	// **
	// STATIC FINALS
	// **

	private final static boolean		WRITE_RESPONSE_TO_DISK			= true;
	private final static String			STATUS_CODE_NOT_OK				= "ERROR: HTTP status code not OK";
	private final static String			UNEXPECTED_CONTENTTYPE			= "ERROR: unexpected content type";
	private final static String			EMPTY_FILE						= "ERROR: file is empty";
	private final static String			CONFIGFILE_LOCATION				= "./src/test/resources/real-collatex-wrap-test.properties";
	private static final String			COLLATEXWRAP_TEST_LOGGER		= "CXWT";

	private static final String			PROPERTY_COLLATEXWRAP_ENDPOINT	= "COLLATEXWRAP_ENDPOINT";
	private static final String			PROPERTY_COLLATEXWRAP_TIMEOUT	= "COLLATEXWRAP_TIMEOUT";
	private static final String			PROPERTY_SESSION_ID				= "SESSION_ID";
	private static final String			LOG_LEVEL						= "INFO";

	private static final String			NO_PREADAPTOR_URI				= "";
	private static final String			NO_POSTADAPTOR_URI				= "";
	private static final List<String>	NO_SPLIT_TAGS					= new ArrayList<String>();
	private static final String			DEFAULT_ALGORITHM				= "dekker";
	private static final String			DEFAULT_COMPARATOR				= "equality";
	private static final int			NO_DISTANCE						= -1;
	private static final boolean		JOINED							= true;

	private static final String			RESPONSE_MIMETYPE_HTML			= "text/html";
	private static final String			RESPONSE_MIMETYPE_JSON			= "application/json";
	private static final String			RESPONSE_MIMETYPE_TEI			= "application/tei+xml";
	private static final String			RESPONSE_MIMETYPE_GRAPHML		= "application/graphml+xml";
	private static final String			RESPONSE_MIMETYPE_DOT			= "text/plain";
	private static final String			RESPONSE_MIMETYPE_SVG			= "image/svg+xml";

	// **
	// STATICS
	// **

	private static TGHttpClient			httpClient;
	private static long					testStartingTime				= System.currentTimeMillis();

	/**
	 * <p>
	 * The logger logs all information with the given loglevel from the
	 * properties file. Please configure the log4j file as stated in the
	 * properties configuration file.
	 * </p>
	 */
	private static Logger				collatexWrapTestLogger			= null;

	// **
	// CLASS
	// **

	private static volatile Properties	configuration					= new Properties();

	// **
	// SET UPS
	// **

	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// Get properties file.
		configuration.load(new FileInputStream(new File(CONFIGFILE_LOCATION)));

		// Get the logger once.
		collatexWrapTestLogger = Logger.getLogger(COLLATEXWRAP_TEST_LOGGER);

		// Do first log.
		collatexWrapTestLogger.info("logging started with logger: "
				+ COLLATEXWRAP_TEST_LOGGER);

		collatexWrapTestLogger.setUseParentHandlers(false);
		Handler fhConsole = new ConsoleHandler();
		fhConsole.setFormatter(new TestConsoleLogFormatter());
		fhConsole.setLevel(Level.parse(LOG_LEVEL));
		collatexWrapTestLogger.addHandler(fhConsole);

		collatexWrapTestLogger.info("\n_______TEST__SETUP_______");

		// Get the HTTP client.
		httpClient = new TGHttpClient(
				configuration.getProperty(PROPERTY_COLLATEXWRAP_ENDPOINT));

		collatexWrapTestLogger.info("server endpoint: "
				+ httpClient.getEndpoint());

		// Set client timeout.
		Integer timeout = Integer.valueOf(configuration
				.getProperty(PROPERTY_COLLATEXWRAP_TIMEOUT));
		httpClient.setClientParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);

		collatexWrapTestLogger.info("client timeout: "
				+ httpClient
						.getClientParameter(CoreConnectionPNames.SO_TIMEOUT));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		//
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		//
	}

	// **
	// TESTS
	// **

	/**
	 * <p>
	 * Use local deployed Juxta web service to test. Just for quick testing of
	 * the service. Needs CollateX-Wrap seevice!
	 * </p>
	 * 
	 * <p>
	 * Get service from:
	 * https://github.com/performant-software/juxta-service.git
	 * </p>
	 * 
	 * <p>
	 * Juxta can not create some text-and-color-output from the CollateX service
	 * TEI parallel segmentation mode! We only could create the HTML from the
	 * TEI using XSLT. This will be documented in the CollateX-Wrap
	 * Documentation!
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	@Ignore
	public void testJuxtaServerWithCollatexTeiParSeg() throws IOException {

		String filename = "xml_miniwenig";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s32k");
		uriList.add("textgrid:1s32m");
		uriList.add("textgrid:1s32n");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_TEI;

		File collateResponse = callService(
				configuration.getProperty(PROPERTY_SESSION_ID), preAdaptorUri,
				NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList, responseMimetype,
				DEFAULT_ALGORITHM, DEFAULT_COMPARATOR, NO_DISTANCE, JOINED,
				filename);

		// **
		// Get the HTTP client.
		// **

		TGHttpClient juxtaClient = new TGHttpClient(
				"http://localhost:8182/juxta/");

		System.out.println(juxtaClient.getEndpoint());

		// **
		// Create an HTTP entity.
		// **

		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.STRICT);
		entity.addPart("sourceName", new StringBody(collateResponse.getName()));
		entity.addPart("contentType", new StringBody("text/xml"));
		FileBody fileBody = new FileBody(collateResponse, "text/xml", "UTF-8");
		entity.addPart("sourceFile", fileBody);

		// **
		// Submit source request.
		// **

		TGHttpResponse response = juxtaClient.post("source", entity);

		System.out.println("[src] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");

		String juxtaId = IOUtils
				.readStringFromStream(response.getInputstream());
		System.out.println("[src] id " + juxtaId);

		// **
		// Import TEI.
		// **

		String jsonRequest = "{ setName: 'new_set_"
				+ System.currentTimeMillis() + "', teiSourceId: " + juxtaId
				+ " }";
		response = juxtaClient.post("import", new StringEntity(jsonRequest),
				"application/json");

		System.out.println("[tps] rq " + jsonRequest);
		System.out.println("[tps] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");
	}

	/**
	 * <p>
	 * Use local deployed Juxta web service to test. Just for quick testing of
	 * the service. No CollateX-Wrap service needed!
	 * </p>
	 * 
	 * <p>
	 * Get service from:
	 * https://github.com/performant-software/juxta-service.git
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	@Ignore
	public void testJuxtaServerWithJuxtaTeiParSeg() throws IOException {

		String filename = "xml_miniwenig";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		// **
		// Get the HTTP client.
		// **

		TGHttpClient juxtaClient = new TGHttpClient(
				"http://localhost:8182/juxta/");

		System.out.println(juxtaClient.getEndpoint());

		// **
		// Create an HTTP entity.
		// **

		String fileName = "./src/test/resources/tei-parseg_juxta-exported.xml";
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.STRICT);
		entity.addPart("sourceName",
				new StringBody(fileName + "_" + System.currentTimeMillis()));
		entity.addPart("contentType", new StringBody("text/plain"));
		FileBody fileBody = new FileBody(new File(fileName), "UTF-8");
		entity.addPart("sourceFile", fileBody);

		// **
		// Submit source request.
		// **

		TGHttpResponse response = juxtaClient.post("source", entity);

		System.out.println("[src] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");

		String juxtaId = IOUtils
				.readStringFromStream(response.getInputstream());
		System.out.println("[src] id " + juxtaId);

		// **
		// Import TEI.
		// **

		String jsonRequest = "{ setName: 'new_set_"
				+ System.currentTimeMillis() + "', teiSourceId: " + juxtaId
				+ " }";
		response = juxtaClient.post("import", new StringEntity(jsonRequest),
				"application/json");

		System.out.println("[tps] rq " + jsonRequest);
		System.out.println("[tps] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");
	}

	/**
	 * <p>
	 * Use local deployed Juxta web service to test. Just for quick testing of
	 * the service. Just Juxta web service is used here, no CollateX-Wrap!
	 * </p>
	 * 
	 * <p>
	 * Get service from:
	 * https://github.com/performant-software/juxta-service.git
	 * </p>
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Test
	@Ignore
	public void testJuxtaServertThreeWitnesses() throws IOException,
			InterruptedException {

		String filename = "three_witnesses";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		// **
		// Get the HTTP client.
		// **

		TGHttpClient juxtaClient = new TGHttpClient(
				"http://localhost:8182/juxta/");

		System.out.println("[edp] " + juxtaClient.getEndpoint());

		// **
		// Create three HTTP entities.
		// **

		String fileName1 = "./src/test/resources/argl1.txt";
		MultipartEntity entity1 = new MultipartEntity(HttpMultipartMode.STRICT);
		entity1.addPart("sourceName",
				new StringBody(fileName1 + "_" + System.currentTimeMillis()));
		entity1.addPart("contentType", new StringBody("text/plain"));
		FileBody fileBody1 = new FileBody(new File(fileName1), "UTF-8");
		entity1.addPart("sourceFile", fileBody1);

		Thread.sleep(77l);
		String fileName2 = "./src/test/resources/argl2.txt";
		MultipartEntity entity2 = new MultipartEntity(HttpMultipartMode.STRICT);
		entity2.addPart("sourceName",
				new StringBody(fileName2 + "_" + System.currentTimeMillis()));
		entity2.addPart("contentType", new StringBody("text/plain"));
		FileBody fileBody2 = new FileBody(new File(fileName2), "UTF-8");
		entity2.addPart("sourceFile", fileBody2);

		Thread.sleep(77l);
		String fileName3 = "./src/test/resources/argl3.txt";
		MultipartEntity entity3 = new MultipartEntity(HttpMultipartMode.STRICT);
		entity3.addPart("sourceName",
				new StringBody(fileName3 + "_" + System.currentTimeMillis()));
		entity3.addPart("contentType", new StringBody("text/plain"));
		FileBody fileBody3 = new FileBody(new File(fileName3), "UTF-8");
		entity3.addPart("sourceFile", fileBody3);

		// **
		// Submit source requests.
		// **

		TGHttpResponse response1 = juxtaClient.post("source", entity1);
		System.out.println("[src] st " + response1.getStatusCode() + " ["
				+ response1.getReasonPhrase() + "]");
		String sourceId1 = IOUtils.readStringFromStream(response1
				.getInputstream());
		sourceId1 = sourceId1.substring(1, (sourceId1.length() - 1));
		System.out.println("[src] id " + sourceId1);

		TGHttpResponse response2 = juxtaClient.post("source", entity2);
		System.out.println("[src] st " + response2.getStatusCode() + " ["
				+ response2.getReasonPhrase() + "]");
		String sourceId2 = IOUtils.readStringFromStream(response2
				.getInputstream());
		sourceId2 = sourceId2.substring(1, (sourceId2.length() - 1));
		System.out.println("[src] id " + sourceId2);

		TGHttpResponse response3 = juxtaClient.post("source", entity3);
		System.out.println("[src] st " + response3.getStatusCode() + " ["
				+ response3.getReasonPhrase() + "]");
		String sourceId3 = IOUtils.readStringFromStream(response3
				.getInputstream());
		sourceId3 = sourceId3.substring(1, (sourceId3.length() - 1));
		System.out.println("[src] id " + sourceId3);

		// **
		// Add witnesses.
		// **

		String jsonRequest1 = "{ source: " + sourceId1 + " }";
		response1 = juxtaClient.post("transform",
				new StringEntity(jsonRequest1), "application/json");
		System.out.println("[wit] rq " + jsonRequest1);
		System.out.println("[wit] st " + response1.getStatusCode() + " ["
				+ response1.getReasonPhrase() + "]");
		String witnessId1 = IOUtils.readStringFromStream(response1
				.getInputstream());
		System.out.println("[wit] id " + witnessId1);

		String jsonRequest2 = "{ source: " + sourceId2 + " }";
		response2 = juxtaClient.post("transform",
				new StringEntity(jsonRequest2), "application/json");
		System.out.println("[wit] rq " + jsonRequest2);
		System.out.println("[wit] st " + response2.getStatusCode() + " ["
				+ response2.getReasonPhrase() + "]");
		String witnessId2 = IOUtils.readStringFromStream(response2
				.getInputstream());
		System.out.println("[wit] id " + witnessId2);

		String jsonRequest3 = "{ source: " + sourceId3 + " }";
		response3 = juxtaClient.post("transform",
				new StringEntity(jsonRequest3), "application/json");
		System.out.println("[wit] rq " + jsonRequest3);
		System.out.println("[wit] st " + response3.getStatusCode() + " ["
				+ response3.getReasonPhrase() + "]");
		String witnessId3 = IOUtils.readStringFromStream(response3
				.getInputstream());
		System.out.println("[wit] id " + witnessId3);

		// **
		// Create comparison set.
		// **

		String jsonRequest = "{ name: 'urgl_" + System.currentTimeMillis()
				+ "', witnesses: [ " + witnessId1 + ", " + witnessId2 + ", "
				+ witnessId3 + " ] }";
		TGHttpResponse response = juxtaClient.post("set", new StringEntity(
				jsonRequest), "application/json");

		System.out.println("[set] rq " + jsonRequest);
		System.out.println("[set] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");

		String setId = IOUtils.readStringFromStream(response.getInputstream());
		System.out.println("[set] id " + setId);

		// **
		// Tokenize witnesses of comparison set.
		// **

		String request = "set/" + setId + "/tokenize";
		response = juxtaClient.post(request, null);

		System.out.println("[tok] rq " + request);
		System.out.println("[tok] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");

		String tokenizeTaskId = IOUtils.readStringFromStream(response
				.getInputstream());
		System.out.println("[tok] id " + tokenizeTaskId);

		// **
		// Check tokenization result.
		// **

		System.out.println("[tsk] waiting...");
		Thread.sleep(1000l);

		request = "task/" + tokenizeTaskId;
		response = juxtaClient.get(request);

		System.out.println("[tsk] rq " + request);
		System.out.println("[tsk] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");

		// **
		// Collate set.
		// **

		System.out.println("[col] waiting...");
		Thread.sleep(1000l);

		request = "set/" + setId + "/collate";
		response = juxtaClient.post(request, null);

		System.out.println("[col] rq " + request);
		System.out.println("[col] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");

		String collationTaskId = IOUtils.readStringFromStream(response
				.getInputstream());
		System.out.println("[col] id " + collationTaskId);

		// **
		// Check collation result.
		// **

		System.out.println("[tsk] waiting...");
		Thread.sleep(1000l);

		request = "task/" + collationTaskId;
		response = juxtaClient.get(request);

		System.out.println("[tsk] rq " + request);
		System.out.println("[tsk] st " + response.getStatusCode() + " ["
				+ response.getReasonPhrase() + "]");
	}

	/**
	 * <p>
	 * Tests the version call.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testVersion() throws IOException {

		collatexWrapTestLogger.info("\n_______TESTING__VERSION_______");

		// Assemble request.
		String request = "/rest/version";

		collatexWrapTestLogger.info("calling "
				+ configuration.getProperty(PROPERTY_COLLATEXWRAP_ENDPOINT)
				+ request);

		// Get response and version string.
		TGHttpResponse response = httpClient.get(request);

		String version = IOUtils.readStringFromStream(response.getBuffEntity()
				.getContent());

		collatexWrapTestLogger
				.info("collatex-wrap service version: " + version);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig plaintext files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextMiniwenig() throws IOException {

		String filename = "plaintext_miniwenig";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn68");
		uriList.add("textgrid:1qn69");
		uriList.add("textgrid:1qn6b");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, true, filename);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig plaintext files with empty
	 * witness(es).
	 * </p>
	 * 
	 * NOTE Use only with CollateX-Server V1.4! EMpty witnesses are allowed
	 * again there!
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextEmptyWitness() throws IOException {

		String filename = "plaintext_miniwenig_empty_witness";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn68");
		uriList.add("textgrid:1qn69");
		// Empty file
		uriList.add("textgrid:1xbn0");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, true, filename);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig plaintext files and JSON response
	 * mimetype.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextMiniwenigJson() throws IOException {

		String filename = "plaintext_miniwenig_json";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn68");
		uriList.add("textgrid:1qn69");
		uriList.add("textgrid:1qn6b");

		String responseMimetype = RESPONSE_MIMETYPE_JSON;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, true, filename);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig plaintext files and XML TEI response
	 * mimetype.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextMiniwenigTei() throws IOException {

		String filename = "plaintext_miniwenig_tei";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn68");
		uriList.add("textgrid:1qn69");
		uriList.add("textgrid:1qn6b");

		String responseMimetype = RESPONSE_MIMETYPE_TEI;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, true, filename);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig plaintext files and GraphML response
	 * mimetype.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextMiniwenigGraphMl() throws IOException {

		String filename = "plaintext_miniwenig_graphml";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn68");
		uriList.add("textgrid:1qn69");
		uriList.add("textgrid:1qn6b");

		String responseMimetype = RESPONSE_MIMETYPE_GRAPHML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, true, filename);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig plaintext files and plaintext
	 * response mimetype (DOT).
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextMiniwenigDot() throws IOException {

		String filename = "plaintext_miniwenig_dot";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn68");
		uriList.add("textgrid:1qn69");
		uriList.add("textgrid:1qn6b");

		String responseMimetype = RESPONSE_MIMETYPE_DOT;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, true, filename);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig plaintext files and SVG response
	 * mimetype.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextMiniwenigSvg() throws IOException {

		String filename = "plaintext_miniwenig_svg";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn68");
		uriList.add("textgrid:1qn69");
		uriList.add("textgrid:1qn6b");

		String responseMimetype = RESPONSE_MIMETYPE_SVG;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, true, filename);
	}

	/**
	 * <p>
	 * Test the collate call with one chapter plaintext files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextOneChapter() throws IOException {

		String filename = "plaintext_one_chapter";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1rqqc");
		uriList.add("textgrid:1rqqd");
		uriList.add("textgrid:1rqqf");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with five chapter plaintext files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollatePlaintextFiveChapters() throws IOException {

		String filename = "plaintext_five_chapters";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:190b8");
		uriList.add("textgrid:190bb");
		uriList.add("textgrid:190bd");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with miniwenig XML files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLMiniwenig() throws IOException {

		String filename = "xml_miniwenig";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s32k");
		uriList.add("textgrid:1s32m");
		uriList.add("textgrid:1s32n");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with one chapter XML files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLOneChapter() throws IOException {

		String filename = "xml_one_chapter";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s328");
		uriList.add("textgrid:1s32b");
		uriList.add("textgrid:1s32c");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with one chapter XML files and Json response
	 * mimetype.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLOneChapterJson() throws IOException {

		String filename = "xml_one_chapter_json";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s328");
		uriList.add("textgrid:1s32b");
		uriList.add("textgrid:1s32c");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_JSON;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with five chapter XML files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLFiveChapters() throws IOException {

		String filename = "xml_five_chapters";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn64");
		uriList.add("textgrid:1qn65");
		uriList.add("textgrid:1qn66");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with five chapter XML files without preprocessor.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLFiveChaptersNoPreprocessor() throws IOException {

		String filename = "xml_five_chapters_no_preprocessor";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1qn64");
		uriList.add("textgrid:1qn65");
		uriList.add("textgrid:1qn66");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with five chapter XML files, edited.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLFiveChaptersEdited() throws IOException {

		String filename = "xml_five_chapters_edited";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1vzzg");
		uriList.add("textgrid:1vzzh");
		uriList.add("textgrid:1vzzj");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with ten chapter XML files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLTenChapters() throws IOException {

		String filename = "xml_ten_chapters";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1vzxq");
		uriList.add("textgrid:1vzxr");
		uriList.add("textgrid:1vzxs");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with ten chapter XML files and tag splitting.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXMLTenChaptersUseTagSplitting() throws IOException {

		String filename = "xml_ten_chapters_tag_splitting";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1vzxq");
		uriList.add("textgrid:1vzxr");
		uriList.add("textgrid:1vzxs");

		List<String> splitTags = new ArrayList<String>();
		splitTags.add("div2");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, splitTags, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with 60 chapter XML files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXML60Chapters() throws IOException {

		String filename = "xml_60_chapters";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s7zw");
		uriList.add("textgrid:1s7zx");
		uriList.add("textgrid:1s7zz");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with 60 chapter XML files and tag splitting.
	 * </p>
	 * 
	 * NOTE Use only with CollateX-Server V1.4! EMpty witnesses are allowed
	 * again there!
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateXML60ChaptersUseTagSplitting() throws IOException {

		String filename = "xml_60_chapters_tag_splitting";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s7zw");
		uriList.add("textgrid:1s7zx");
		uriList.add("textgrid:1s7zz");

		List<String> splitTags = new ArrayList<String>();
		splitTags.add("div2");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, splitTags, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with 100 chapter XML files.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	@Ignore
	public void testCollateXML100Chapters() throws IOException {

		String filename = "xml_100_chapters";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s801");
		uriList.add("textgrid:1s802");
		uriList.add("textgrid:1s803");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with 100 chapter XML files using tag splitting with
	 * multiple split tags.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	@Ignore
	public void testCollateXML100ChaptersUseMultipleTagSplitting()
			throws IOException {

		String filename = "xml_100_chapters_use_tag_splitting";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1s801");
		uriList.add("textgrid:1s802");
		uriList.add("textgrid:1s803");

		List<String> splitTags = new ArrayList<String>();
		splitTags.add("div2");
		splitTags.add("div3");

		String preAdaptorUri = "textgrid:1s0m0";
		String responseMimetype = RESPONSE_MIMETYPE_HTML;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				preAdaptorUri, NO_POSTADAPTOR_URI, splitTags, uriList,
				responseMimetype, DEFAULT_ALGORITHM, DEFAULT_COMPARATOR,
				NO_DISTANCE, JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with one chapter plaintext files and decker
	 * experimental algorithm.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateDeckerExp() throws IOException {

		String filename = "plaintext_one_chapter_decker_experimental";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1rqqc");
		uriList.add("textgrid:1rqqd");
		uriList.add("textgrid:1rqqf");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;
		String algorithm = "decker-experimental";

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, algorithm, DEFAULT_COMPARATOR, NO_DISTANCE,
				JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with one chapter plaintext files and MEDITE
	 * algorithm.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateMedite() throws IOException {

		String filename = "plaintext_one_chapter_medite";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1rqqc");
		uriList.add("textgrid:1rqqd");
		uriList.add("textgrid:1rqqf");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;
		String algorithm = "medite";

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, algorithm, DEFAULT_COMPARATOR, NO_DISTANCE,
				JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with one chapter plaintext files and
	 * needleman-wunsch algorithm.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateNeedlemanWunsch() throws IOException {

		String filename = "plaintext_one_chapter_needleman-wunsch";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:1rqqc");
		uriList.add("textgrid:1rqqd");
		uriList.add("textgrid:1rqqf");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;
		String algorithm = "needleman-wunsch";

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, algorithm, DEFAULT_COMPARATOR, NO_DISTANCE,
				JOINED, filename);
	}

	/**
	 * <p>
	 * Test the collate call with five chapter plaintext files and levenshtein
	 * comparator and distance 4.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateLevenshteinDistance4() throws IOException {

		String filename = "plaintext_levenshtein_comparator_distance_4";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:190b8");
		uriList.add("textgrid:190bb");
		uriList.add("textgrid:190bd");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;
		String algorithm = "dekker";
		String comparator = "levenshtein";
		int distance = 4;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, algorithm, comparator, distance, JOINED,
				filename);
	}

	/**
	 * <p>
	 * Test the collate call with five chapter plaintext files and levenshtein
	 * comparator and distance 10.
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	public void testCollateLevenshteinDistance10() throws IOException {

		String filename = "plaintext_levenshtein_comparator_distance_10";

		collatexWrapTestLogger.info("\n_______TESTING__"
				+ filename.toUpperCase() + "_______");

		List<String> uriList = new ArrayList<String>();
		uriList.add("textgrid:190b8");
		uriList.add("textgrid:190bb");
		uriList.add("textgrid:190bd");

		String responseMimetype = RESPONSE_MIMETYPE_HTML;
		String algorithm = "dekker";
		String comparator = "levenshtein";
		int distance = 10;

		callService(configuration.getProperty(PROPERTY_SESSION_ID),
				NO_PREADAPTOR_URI, NO_POSTADAPTOR_URI, NO_SPLIT_TAGS, uriList,
				responseMimetype, algorithm, comparator, distance, JOINED,
				filename);
	}

	/**
	 * <p>
	 * Test the collate call with different post adaptor URIs (if XML).
	 * </p>
	 * 
	 * @throws IOException
	 */
	@Test
	@Ignore
	public void testPostAdaoptors() throws IOException {
		// TEI2HTML.
		// String postAdaptorUri = "textgrid:1vzvm";
		// GraphML2HTML.
		// String postAdaptorUri = "textgrid:1vzxn";
	}

	/**
	 * <p>
	 * Calls the CollateX-Wrap Service.
	 * </p>
	 * 
	 * @param theSessionId
	 * @param thePreAdaptorUri
	 * @param thePostAdaptorUri
	 * @param theSplitTags
	 * @param theUris
	 * @param theResponseMimetype
	 * @param theAlgorithm
	 * @param theComparator
	 * @param theDistance
	 * @param theJoined
	 * @param theFilename
	 * @return
	 * @throws IOException
	 */
	private static File callService(String theSessionId,
			String thePreAdaptorUri, String thePostAdaptorUri,
			List<String> theSplitTags, List<String> theUris,
			String theResponseMimetype, String theAlgorithm,
			String theComparator, int theDistance, boolean theJoined,
			String theFilename) throws IOException {

		File result = null;

		// Get start time.
		long duration = System.currentTimeMillis();

		// Assemble request.
		StringBuffer request = new StringBuffer();
		request.append("/rest/collate?sessionId=" + theSessionId);
		if (thePreAdaptorUri != null && !thePreAdaptorUri.equals("")) {
			request.append("&preAdaptorUri=" + thePreAdaptorUri);
		}
		if (thePostAdaptorUri != null && !thePostAdaptorUri.equals("")) {
			request.append("&postAdaptorUri=" + thePostAdaptorUri);
		}
		if (theSplitTags != null && !theSplitTags.isEmpty()) {
			for (String s : theSplitTags) {
				request.append("&splitTag=" + s);
			}
		}
		for (String u : theUris) {
			request.append("&uri=" + u);
		}
		request.append("&responseMimetype="
				+ URLEncoder.encode(theResponseMimetype, "UTF-8"));
		request.append("&algorithm=" + theAlgorithm);
		request.append("&comparator=" + theComparator);
		if (theDistance != NO_DISTANCE) {
			request.append("&distance=" + theDistance);
		}
		request.append("&joined=" + theJoined);

		// Print parameters.
		collatexWrapTestLogger.info("parameters:");
		collatexWrapTestLogger.info("\tsessionId:       " + theSessionId);
		collatexWrapTestLogger.info("\tpreAdaptorUri:   " + thePreAdaptorUri);
		collatexWrapTestLogger.info("\tpostAdaptorUri:  " + thePostAdaptorUri);
		collatexWrapTestLogger.info("\tsplitTags:       " + theSplitTags);
		collatexWrapTestLogger.info("\turiList:         " + theUris);
		collatexWrapTestLogger
				.info("\treponseMimetype: " + theResponseMimetype);
		collatexWrapTestLogger.info("\talgorithm:       " + theAlgorithm);
		collatexWrapTestLogger.info("\tcomparator:      " + theComparator);
		if (theComparator.equals("levenshtein")) {
			collatexWrapTestLogger.info("\tdistance:        " + theDistance);
		}
		collatexWrapTestLogger.info("\tjoined:          " + theJoined);

		// Call the service.

		collatexWrapTestLogger.info("calling "
				+ configuration.getProperty(PROPERTY_COLLATEXWRAP_ENDPOINT)
				+ request);

		// Get the response and it's status.
		TGHttpResponse response = httpClient.get(request.toString(), null);

		int statusCode = response.getStatusCode();
		String reasonPhrase = response.getReasonPhrase();

		BufferedHttpEntity entity = response.getBuffEntity();
		String responseContentType = entity.getContentType().getValue();

		collatexWrapTestLogger.info("status: " + statusCode + " / "
				+ reasonPhrase);

		// Check Status code.
		if (response.getStatusCode() != HttpStatus.SC_OK) {
			collatexWrapTestLogger.info(STATUS_CODE_NOT_OK
					+ IOUtils.readStringFromStream(response.getInputstream()));
			collatexWrapTestLogger.info("response: "
					+ IOUtils.toString(entity.getContent()));
			assertTrue(false);
		}

		// Test if we have got the expected content type (for the collatex-wrap
		// service returns text/xml instead of the two others...
		if (responseContentType.equals("text/xml")
				&& (theResponseMimetype.equals("application/tei+xml"))
				|| theResponseMimetype.equals("application/graphml+xml")) {
			// Everything is correct!
		} else if (!responseContentType.equals(theResponseMimetype)) {
			collatexWrapTestLogger.info(UNEXPECTED_CONTENTTYPE + " "
					+ responseContentType + ", expected was: "
					+ theResponseMimetype);
			assertTrue(false);
		}

		// Write to file if configured.
		if (statusCode == HttpStatus.SC_OK && WRITE_RESPONSE_TO_DISK) {

			File f = new File("./src/test/resources/files/"
					+ String.valueOf(testStartingTime) + "." + theFilename
					+ ".xml");
			FileOutputStream fos = new FileOutputStream(f);
			entity.writeTo(fos);
			fos.close();

			collatexWrapTestLogger.info("content-type: " + responseContentType);
			collatexWrapTestLogger.info("written to: " + f.getName());
			collatexWrapTestLogger.info("file size: " + f.length());

			// Test if we have null content.
			if (f.length() == 0) {
				collatexWrapTestLogger.info(EMPTY_FILE);
				assertTrue(false);
			}

			result = f;

			// Do not log content here.
			// collatexWrapTestLogger.info("response: " +
			// IOUtils.toString(entity.getContent());
		}

		// Print duration.
		long durationInSecs = ((System.currentTimeMillis() - duration) / 1000);
		collatexWrapTestLogger.info("duration: " + durationInSecs + " second"
				+ (durationInSecs != 1 ? "s" : ""));

		return result;
	}

}
