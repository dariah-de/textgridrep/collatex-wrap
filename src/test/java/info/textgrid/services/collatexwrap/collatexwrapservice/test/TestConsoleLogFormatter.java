/*******************************************************************************
 * This software is copyright (c) 2013 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www-daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.services.collatexwrap.collatexwrapservice.test;

import java.util.logging.LogRecord;

/*******************************************************************************
 * PACKAGE
 * 			info.textgrid.services.collatexwrap.collatexwrapservice.test
 * 
 * FILE
 * 			TestConsoleLogFormatter.java
 * 
 *******************************************************************************
 * TODOLOG
 * 	
 *******************************************************************************
 * CHANGELOG
 *
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * The testConsoleLogFormatter formats the test's console log.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2013-05-07
 * @since 2013-05-07
 ******************************************************************************/

public class TestConsoleLogFormatter extends java.util.logging.Formatter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	@Override
	public String format(LogRecord record) {
		return record.getMessage() + "\n";
	}

}
