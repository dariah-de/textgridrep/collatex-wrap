Synopsis

The Juxta family of software (Juxta, Juxta WS, and Juxta Commons) allows you to compare and collate versions of the same graphical work. The Juxta Web Service (Juxta WS) is an open source Java application that provides the core collation and visualization functions of Juxta in a server environment via an API. Development of Juxta WS was supported by NINES at the University of Virginia.
